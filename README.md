## Airflow Docker image that starts the web-server preconfigured with an admin user

### Features
0. Out of the box login with admin user
1. Run with SequenceExecutor
2. Run with LocalExecutor
3. Run with CeleryExecutor
4. Use SQLlite DB
5. Use postgres DB
6. Use mysql DB [WIP]

### Usage with docker run

#### 1. sequence executor + admin-login + sqllite-db
```bash
sudo docker run --rm --name airflow -p 8080:8080 -v /home/ubuntu/my_projects/airflow/dags:/usr/local/airflow/dags -e ADMIN_PASSWORD=test -e FERNET_KEY=XXXX -e AIRFLOW__WEBSERVER__RBAC=true -e AIRFLOW__WEBSERVER__WORKERS=2 -e AIRFLOW__WEBSERVER__WORKER_REFRESH_INTERVAL=1800 -e AIRFLOW__WEBSERVER__WEB_SERVER_WORKER_TIMEOUT=300 mannharleen/airflow:1.10.2
```

#### 2. local executor + admin-login + sqllite-db 
```bash
*** ERROR: airflow.exceptions.AirflowConfigException: error: cannot use sqlite with the LocalExecutor
```

#### 3. local executor + admin-login + postgres 
```
sudo docker run --rm --name airflow -p 8080:8080 -v /home/ubuntu/my_projects/airflow/dags:/usr/local/airflow/dags -e ADMIN_PASSWORD=test -e FERNET_KEY=XXXX -e AIRFLOW__WEBSERVER__RBAC=true -e AIRFLOW__WEBSERVER__WORKERS=2 -e AIRFLOW__WEBSERVER__WORKER_REFRESH_INTERVAL=1800 -e AIRFLOW__WEBSERVER__WEB_SERVER_WORKER_TIMEOUT=300 -e EXECUTOR=Local  -e AIRFLOW__CORE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@postgres/airflow mannharleen/airflow:1.10.2
# *** see docker-compose on github file if you want to run postgres as well
```

### Usage with docker-compose
Edit the environment section in docker-compose-LocalExecutor.yml, and run
```bash
sudo docker-compose -f docker-compose-LocalExecutor.yml -d
```

### Check if user has been created and correct DB is being used
```bash
# sudo docker exec -it airflow_webserver_1 bash
airflow list_users
```

> PS. Largely motivated by https://github.com/puckel/docker-airflow

