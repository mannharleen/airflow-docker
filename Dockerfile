FROM python:3.6-slim
LABEL maintainer="mannharleen"

### helper commands
# > sudo docker build -t mannharleen/airflow:1.10.2 .
# Generate fernet key > openssl rand -base64 32
# > sudo docker run --rm --name airflow -p 8080:8080 -e ADMIN_PASSWORD=test -e FERNET_KEY=bZb9yF5/gGQQnteJnFN5zTtw08AXvbKatzngdVxovT8= -e AIRFLOW__WEBSERVER__RBAC=true -e AIRFLOW__WEBSERVER__WORKERS=2 -e AIRFLOW__WEBSERVER__WORKER_REFRESH_INTERVAL=1800 -e AIRFLOW__WEBSERVER__WEB_SERVER_WORKER_TIMEOUT=300 mannharleen/airflow:1.10.2
# >> (add if postgre available) -e AIRFLOW__CORE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@postgres/airflow
# >> (add if want to change executor) -e EXECUTOR=Local or Sequential or Celery
#
# Examples:
#
# 1. sequence executor + admin-login + sqllite-db
# > sudo docker run --rm --name airflow -p 8080:8080 -v /home/ubuntu/my_projects/airflow/dags:/usr/local/airflow/dags -e ADMIN_PASSWORD=test -e FERNET_KEY=bZb9yF5/gGQQnteJnFN5zTtw08AXvbKatzngdVxovT8= -e AIRFLOW__WEBSERVER__RBAC=true -e AIRFLOW__WEBSERVER__WORKERS=2 -e AIRFLOW__WEBSERVER__WORKER_REFRESH_INTERVAL=1800 -e AIRFLOW__WEBSERVER__WEB_SERVER_WORKER_TIMEOUT=300 mannharleen/airflow:1.10.2
#
# 2. local executor + admin-login + sqllite-db *** ERROR: airflow.exceptions.AirflowConfigException: error: cannot use sqlite with the LocalExecutor
#
# 3. local executor + admin-login + postgres *** see docker-compose file for starting postgres
# > sudo docker run --rm --name airflow -p 8080:8080 -v /home/ubuntu/my_projects/airflow/dags:/usr/local/airflow/dags -e ADMIN_PASSWORD=test -e FERNET_KEY=bZb9yF5/gGQQnteJnFN5zTtw08AXvbKatzngdVxovT8= -e AIRFLOW__WEBSERVER__RBAC=true -e AIRFLOW__WEBSERVER__WORKERS=2 -e AIRFLOW__WEBSERVER__WORKER_REFRESH_INTERVAL=1800 -e AIRFLOW__WEBSERVER__WEB_SERVER_WORKER_TIMEOUT=300 -e EXECUTOR=Local  -e AIRFLOW__CORE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@postgres/airflow mannharleen/airflow:1.10.2

# Never prompts the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Airflow
ARG AIRFLOW_VERSION=1.10.2
ARG AIRFLOW_HOME=/usr/local/airflow
ARG AIRFLOW_DEPS=""
ARG PYTHON_DEPS=""
ENV AIRFLOW_GPL_UNIDECODE yes

# Define en_US.
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_MESSAGES en_US.UTF-8

RUN set -ex \
    && buildDeps=' \
        freetds-dev \
        libkrb5-dev \
        libsasl2-dev \
        libssl-dev \
        libffi-dev \
        libpq-dev \
        git \
    ' \
    && apt-get update -yqq \
    && apt-get upgrade -yqq \
    && apt-get install -yqq --no-install-recommends \
        $buildDeps \
        freetds-bin \
        build-essential \
        default-libmysqlclient-dev \
        apt-utils \
        curl \
        rsync \
        libpq5 \
        netcat \
        locales \
    && sed -i 's/^# en_US.UTF-8 UTF-8$/en_US.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen \
    && update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 \
    && useradd -ms /bin/bash -d ${AIRFLOW_HOME} airflow \
    && pip install -U pip setuptools wheel \
    && pip install pytz \
    && pip install pyOpenSSL \
    && pip install ndg-httpsclient \
    && pip install pyasn1 \
    && pip install apache-airflow[crypto,celery,postgres,hive,jdbc,mysql,ssh${AIRFLOW_DEPS:+,}${AIRFLOW_DEPS}]==${AIRFLOW_VERSION} \
    && pip install 'redis==3.2.1' \
    && if [ -n "${PYTHON_DEPS}" ]; then pip install ${PYTHON_DEPS}; fi \
    && apt-get purge --auto-remove -yqq $buildDeps \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man \
        /usr/share/doc \
        /usr/share/doc-base

COPY script/entrypoint.sh /entrypoint.sh
COPY config/airflow.cfg ${AIRFLOW_HOME}/airflow.cfg
COPY config/webserver_config.py ${AIRFLOW_HOME}/webserver_config.py

RUN chown -R airflow: ${AIRFLOW_HOME}
RUN ls -lrt /
EXPOSE 8080 5555 8793

USER airflow
WORKDIR ${AIRFLOW_HOME}
ENTRYPOINT ["/entrypoint.sh"]
CMD ["webserver"] # set default arg for entrypoint
